module "cloud-dns" {
  source      = "terraform-google-modules/cloud-dns/google"
  version     = "~> 4.1.0"
  description = var.description
  project_id  = var.project_id
  type        = "public"
  name        = var.zone_name
  domain      = var.domain

  private_visibility_config_networks = [
    "https://www.googleapis.com/compute/v1/projects/my-project/global/networks/my-vpc"
  ]

  recordsets = [
    /* {
      name = "localhost"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "MX"
      ttl  = 300
      records = [
        "1 localhost.",
      ]
    },
    {
      name = ""
      type = "TXT"
      ttl  = 300
      records = [
        "\"v=spf1 -all\"",
      ]
    }, */
  ]
  # insert the 3 required variables here
}
