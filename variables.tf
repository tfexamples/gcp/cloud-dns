variable "credentials" {
  type        = string
  description = "Location of the credential keyfile."
}

variable "region" {
  type        = string
  description = "Region of the domain zone."
}

variable "description" {
  type        = string
  description = "Brief description of the domain zone."
}

variable "project_id" {
  type        = string
  description = "The project ID of the domain."
}

variable "zone_name" {
  type        = string
  description = "The name of the Domain zone"
}

variable "domain" {
  type        = string
  description = "The name of the Domain."
}
