output "name_servers" {
  description = "Zone name servers."
  value       = module.cloud-dns.name_servers
}
